import { Previewer, Handler } from '../../pagedjs/paged.esm.js'


function handlersFunction(previewer){

  previewer.registerHandlers(
    class CustomHandlers extends Handler {

      /* your custom javascript here ----------------------------------------------------- */
      

      beforeParsed(content){
        // console.log(content);
      }

      afterPageLayout(pageElement, page, breakToken){
        // console.log(pageElement);
      }


      /* --------------------------------------------------------------------------------- */

    
    },
  );

}


export { handlersFunction };
