/* Add custom fonts here ------------------------------------------------------ */

let fonts = [
    'src/fonts/fira-sans/stylesheet.css',
    'https://fonts.googleapis.com/css2?family=Open+Sans&display=swap'
]

/* ----------------------------------------------------------------------------- */



function addFonts(){
    let head = document.getElementsByTagName('HEAD')[0]; 
    for(let i = 0; i < fonts.length; i++){
      let link = document.createElement('link'); 
      link.rel = 'stylesheet';  
      link.type = 'text/css'; 
      link.href = fonts[i];  
      head.appendChild(link);
    }
    
}

export { addFonts };

