Template Pajed.js de Julie Blanc 
https://julie-blanc.fr


# Paged.js workshop template

Paged.js template for multi-media publishing.
- CSS for screen and print 
- Paged.js 0.1.42 included
- Script for register your handlers 
- Waits for the page to be returned before printing


## Instructions

- Use any local server.
- Write your content in `src/content.html`
- Write your CSS for the screen in `src/css/style-screen.css` and your CSS for the print in `src/css/style-print.css`
- To add fonts, put your fonts files in `src/fonts` and add link to files into `src/fonts/fonts.js`
- If you need to register handlers, you can do it in `src/js/handlers.js`



## Version
This template was made by Julie Blanc for workshops. It's in constant improvement.

- v 1.0: Grand Atelier ESAD Orléans, January 27-31, 2020
- v 2.0: 10/10/2020

## Licence

MIT Licence